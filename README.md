This is work around on Chapter 8 Test-Driven Development of Stefan Roth's book [1].


# This repository contains workaroud:

1. branch the_first_test - contains first test and dummy function which is under the test

![Simplest Unit Test with Google Test](/.img/step_one.png "Simplest Unit Test with Google Test")

2. branch the_second_test - contains two tests and dummy implementation of the function which is under the test

![Again an simplest Unit Test with Google Test](/.img/step_two.png "Again an simplest Unit Test with Google Test")

3. branch the_third_test - contains more tests and classical implementation with two while-loops of the function which is under the test

![More Unit Tests with Google Test](/.img/step_three.png "More Unit Tests with Google Test")

4. branch custom_assertion - contains improved test implementation with dedicated class RomanNumeralAsser and method isConvertedToRomanNumeral, from the code side, there is a classical implementation with several of while-loops in the function which is under the test.

![Improved Implementation of Tests with Google Test](/.img/step_four.png "[Improved Implementation of Tests with Google Test")

5. branch Step_five_a_refactoring_time - contains the same test strategy and advanced implementation of the function under the test. Firstly a struct ArabicToRomanMapping is introduced which allows mapping Arabic numbers to Roman Numbers and an array that is used by the function under the test.

![Improved Implementation of Tests with Google Test](/.img/step_five.png "[Improved Implementation of Tests with Google Test")


6. branch Step_six_approaching_the_finish_line - contains the same test strategy and full implementation of the function under the test.

7. branch main - contains full work

Also repository contains CI to build and test the source code.

#What I have learned from this chapter? 

## The Advantages of Test-Driven Development are the following:

1. TDD, if done right, forces you to take small steps when writing software.
2. TDD establishes a very fast feedback loop.
3. Creating a unit test first helps a developer to really consider what needs to be
done.
4. With TDD a gapless specification arises in the form of executable code.
5. The developer deals much more consciously and responsibly with dependencies.
6. The emerging production code with TDD will have 100% unit test coverage
by default.

Tip: If you want to dive deeper into Test-Driven Development with C++, Stefan Roth's recommendation is an excellent book
Modern C++ Programming with Test-Driven Development by Jeff Langr. This book offers much deeper insights into TDD and gives you hands-on lessons in the challenges and rewards of doing TDD in C++.

## When We Should Not Use TDD

As Stefan Roth says in his book: "But some parts of a project are so simple, small, or less complex, that it doesn’t justify this approach. If you can write your code quickly off the cuff, because complexity and risks are low, then of course you can do that."
### Do not use TDD if you are developing:
1. data classes without functionality
2. simple glue code that just couples together with two modules.
###  Avoid if you can to use TDD when you are working:
1. very innovative environment without domain experience
2. very volatile and fuzzy requirements

Any way the Unit Tests can be retrofitted in the project later if needed.

As Stefan Roth clearly states in this chapter:  Another big challenge, for which TDD won’t help, is getting a good architecture. TDD does not replace the necessary reflecting on the coarse-grained structures (subsystems, components, …) of your software system. If you are faced with fundamental decisions about frameworks, libraries, technologies, or architecture patterns, TDD will not help you.

# How to build on your PC ?

Local building with cmake:


```
my_project$ cmake -S . -B build
-- The C compiler identification is GNU 10.2.1
-- The CXX compiler identification is GNU 10.2.1
...
-- Build files have been written to: .../my_project/build

my_project$ cmake --build build
Scanning dependencies of target gtest
...
[100%] Built target gmock_main

my_project$ cd build && ctest
```


# Where to buy:

https://www.amazon.fr/Clean-Sustainable-Software-Development-Practices/dp/1484227921


# Bibliography:

[1] Roth S 2017 Clean C++: Sustainable Software Development Patterns and Best Practices with C++ (New York: Apress)

[2] Langr J 2013 Modern C++ Programming with Test-Driven Development

[3] https://carbon.now.sh/
